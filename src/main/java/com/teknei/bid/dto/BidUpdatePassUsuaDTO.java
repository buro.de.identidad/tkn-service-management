package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidUpdatePassUsuaDTO implements Serializable{
	private static final long serialVersionUID = -335996902177482083L;
	private int tipoOper; //0 = vigencia, 1 = recuperacion;
    private String username;
    private String newPassword;
    private String oldPassword;
}