package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class BidAssignmentRequestDTO implements Serializable {

    private Long idCustomer;
    private Long idCompany;
    private Long idDisp;
    private String username;

}